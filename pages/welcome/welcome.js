// pages/welcome/welcome.js
import {
  getAccountPhone,
  userLogin,
  loginByPassword
} from '../../request/getChenyundata'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    params: {
      phone: '',
      password: ''
    }
  },
  getusername(e) {
    console.log(e.detail.value)
    this.data.params.phone = e.detail.value
  },
  getuserpwd(e) {
    console.log(e.detail.value)
    this.data.params.password = e.detail.value
  },
  loginIn() {
    loginByPassword(this.data.params).then(r => {
      if(r.statusCode==200){
        if(r.data.error){
          wx.showToast({
            title: r.data.error.message,
            icon:'none'
          })
        }else{
          wx.setStorageSync('token', r.data.result.token)
          // let arr = r.data.result.functions.filter(fun => {
          //   return fun.parentFuncitonId == 1
          // })
          let home = r.data.result.functions.filter(fun => {
            return fun.parentFuncitonId == 0
          })
          wx.setStorageSync('modules', JSON.stringify(r.data.result.functions))
          wx.setStorageSync('userInfo', JSON.stringify(r.data.result.accountEntity))
          wx.setStorageSync('homes', JSON.stringify(home))
          wx.reLaunch({
            url: '/pages/home/home',
          })
        }
      }else{
        wx.showToast({
          title: '请输入正确的用户名密码',
          icon:'none'
        })
      }
    })
  },
  phone: function (e) {
    if (e.detail.errMsg == 'getPhoneNumber:ok') {
      let par = {
        encryptedData: e.detail.encryptedData,
        iv: e.detail.iv,
        sessionKey: wx.getStorageSync('sessionKey')
      }
      getAccountPhone(par).then(res => {
        let p = {
          phone: res.data.result.phoneNumber
        }
        userLogin(p).then(r => {
          wx.setStorageSync('token', r.data.result.token)

          let home = r.data.result.functions.filter(fun => {
            return fun.parentFuncitonId == 0
          })
          console.log(r.data.result.functions)
          wx.setStorageSync('modules', JSON.stringify(r.data.result.functions))
          wx.setStorageSync('homes', JSON.stringify(home))
          wx.reLaunch({
            url: '/pages/home/home',
          })
        })
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})