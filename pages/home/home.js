// pages/home/home.js
import {
  getAccountPhone,
  userLogin
} from '../../request/getChenyundata'
Page({

  /**
   * 页面的初始数据
   */

  data: {
    homeBtnList: []
  },
  toDetail: function (e) {
    console.log(e)
    // console.log(e.currentTarget.dataset.url)
    // if (e.currentTarget.dataset.index == 2) {
    //   wx.showToast({
    //     title: '暂未开通',
    //     icon: 'none',
    //     duration: 1000
    //   })
    // } else {
    if (e.currentTarget.dataset.index == 3) {
      wx.navigateTo({
        url: `${e.currentTarget.dataset.url}`
      })
    } else {
      wx.navigateTo({
        url: `${e.currentTarget.dataset.url}&name=${e.currentTarget.dataset.name}`
      })
    }

    // }

  },
  tipfun: function () {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (wx.getStorageSync('homes')) {
      this.setData({
        homeBtnList: JSON.parse(wx.getStorageSync('homes'))
      })
    } else {
      wx.reLaunch({
        url: '/pages/welcome/welcome',
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})