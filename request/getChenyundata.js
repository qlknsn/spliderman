import reqeust from './chengyunRequest'
export function jscode2session(params) {
  return reqeust({
    url: '/public/api/seaweed/v1/account/jscode2session',
    data:params
  })
}
export function getAccountPhone(params) {
  return reqeust({
    url: '/public/api/seaweed/v1/account/phone',
    data:params
  })
}
export function userLogin(params) {
  return reqeust({
    url: '/public/api/seaweed/v1/account/login?phone='+params.phone,
  })
}
export function taskList(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/seaweed/v1/task/list',
    method:'post',
    data:params,
    header:header
  })
}
export function handleTask(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/seaweed/v1/task/handle',
    method:'post',
    data:params,
    header:header
  })
}
export function taskInfo(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/seaweed/v1/task/info',
    data:params,
    header:header
  })
}
export function sentMessage(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/seaweed/v1/task/sentMessage',
    data:params,
    header:header
  })
}
export function loginByPassword(params) {
  return reqeust({
    url: '/public/api/seaweed/v1/account/loginByPassword',
    data:params,
  })
}
// 问题上报的列表
export function getQuestionHandleList(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/oldStreet/v1/question/getQuestionHandle',
    data:params,
    header:header
  })
}
// 问题上报的详情
export function getQuestionOne(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/oldStreet/v1/question/getQuestionOne',
    data:params,
    header:header
  })
}
// 问题上报的处置
export function handleQuestion(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: '/public/api/oldStreet/v1/question/handleQuestion',
    method:'post',
    data:params,
    header:header
  })
}