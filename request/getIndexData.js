import reqeust from './request'
export function getTaskStataus() {
  return reqeust({
    url: 'public/api/seaweed/v1/order/statuTypes',
  })
}
export function getTaskList(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: 'public/api/seaweed/v1/order/list',
    method: 'post',
    data: params,
    header:header
  })
}
export function handleTask(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  console.log()
  return reqeust({
    url: 'public/api/seaweed/v1/order/handle',
    method: 'post',
    data:params,
    header:header
  })
}
export function taskdetail(params) {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: 'public/api/seaweed/v1/order/detail?id='+params.id,
    header:header
  })
}
export function configList() {
  let header = {
    token: wx.getStorageSync('token')
  }
  return reqeust({
    url: 'public/api/seaweed/v1/config/list',
    header:header
  })
}