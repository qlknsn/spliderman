// pages/taskList/taskList.js
// import {
//   getTaskStataus,
//   getTaskList,
//   configList
// } from '../../request/getIndexData'
import {
  taskList,
  sentMessage,
  getQuestionHandleList
} from '../../request/getChenyundata'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modelTitle:'',
    nodata: false,
    API_HOST: 'https://bh-sanlin.oss-cn-shanghai.aliyuncs.com/',
    list: [{
        "text": "对话",
        "iconPath": "../../images/tabbar_icon_chat_default.png",
        "selectedIconPath": "../../images/tabbar_icon_chat_active.png",
        dot: true
      },
      {
        "text": "设置",
        "iconPath": "../../images/tabbar_icon_setting_default.png",
        "selectedIconPath": "../../images/tabbar_icon_setting_active.png",
        badge: 'New'
      }
    ],
    statusList: [],
    taskList: [],
    index: 0,
    status: 0,
    startTime: '',
    array: [{
      message: 'foo',
    }, {
      message: 'bar'
    }],
    total: 0,
    page: 0,
    pickerVisible: false,
    isMore: true,
    modelType: '',
    dateText: '请选择开始日期'
  },
  sentphoneMessage(e){
    console.log(e.currentTarget.dataset.item.taskId)
    sentMessage({taskId:e.currentTarget.dataset.item.taskId}).then(res=>{
      if(res.data.error){
        wx.showToast({
          title: '发送通知失败请重试',
          icon: 'none',
          duration: 2000
        })
      }else{
        wx.showToast({
          title: '发送通知成功',
          icon: 'none',
          duration: 2000
        })
      }
    })
  },
  myevent(e) {
    if (e.detail.params) {
      this.setData({
        startTime: e.detail.params.startdate,
        endTime: e.detail.params.enddate,
        page: 1,
        taskList: []
      })
      this.getTaskLists()
    }
    this.setData({
      pickerVisible: false
    })
  },
  tabChange(e) {
    console.log('tab change', e)
  },
  openPicker() {
    this.setData({
      pickerVisible: true
    })
  },
  bindPickerChange: function (e) {
    let int = 0
    for (let i = 0; i < this.data.statusList.length; i++) {
      if (e.detail.value == i) {
        int = this.data.statusList[i].code
      }
    }
    this.setData({
      index: e.detail.value,
      status: int,
      page: 1,
      taskList: []
    })
    this.getTaskLists()
  },
  bindDateChange: function (e) {
    this.setData({
      startTime: e.detail.value,
      dateText: '请选择结束日期'
    })
  },
  bindKeyInput: function (e) {
    this.setData({
      inputValue: e.detail.value,
      page: 1,
      taskList: []
    })
    this.getTaskLists()
  },
  toDetail: function (e) {
    wx.navigateTo({
      url: '/pages/detail/detail?params=' + JSON.stringify(e.currentTarget.dataset.content)
    })
  },
  getTaskLists: function () {
    let params = {
      "start": this.data.page || 0,
      "count": 10,
      "type": this.data.status || 0,
    }
    if (this.data.inputValue) {
      params.content = this.data.inputValue
    }
    // if (this.data.status) {
    //   params.status = this.data.status
    // }
    if (this.data.startTime) {
      params.startTime = this.data.startTime
    }
    if (this.data.endTime) {
      params.endTime = this.data.endTime
    }
    getQuestionHandleList(params).then(res => {
      if(this.data.page==0){
        this.setData({
          taskList:[]
        })
      }
      this.setData({
        taskList: [...this.data.taskList, ...res.data.results],
        total: res.data.pagination.total
      })
      if (this.data.page * 10 < this.data.total) {
        this.setData({
          isMore: true,
          page: (this.data.page + 1)*10
        })
      } else {
        this.setData({
          isMore: false
        })
      }
    }).catch(err => console.log(err))
  },
  toDetail: function (e) {
    console.log(e)
    let obj = {
      uniqueId: e.currentTarget.dataset.content.uniqueId,
    }
    if (e.currentTarget.dataset.content.type == 0) {
      wx.navigateTo({
        url: '/chengyun/laojietaskHandle/taskHandle?params=' + JSON.stringify(obj)
      })
    } else {
      wx.navigateTo({
        url: '/chengyun/laojietaskDetail/taskDetail?params=' + JSON.stringify(obj)
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    // configList().then(res=>{
    //   this.setData({
    //     API_HOST:res.data.result.previewUrl
    //   })
    // })
    // let modelType = ''
    // wx.getStorage({
    //   key: 'modelType',
    //   success (res) {
    //     modelType = res.data
    //   }
    // })
    
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(options)
        // 获取当前小程序的页面栈
  let pages = getCurrentPages();
  // 数组中索引最大的页面--当前页面
  let currentPage = pages[pages.length-1];
  let options = currentPage.options
    this.setData({
      modelType: Number(options.type)
    })
    this.setData({
      modelTitle: options.title
    })
    wx.setNavigationBarTitle({
      title: options.title
    })
    // wx.setStorage({
    //   key:"modelType",
    //   data:options.type
    // })

    this.setData({
      statusList: [{
          code: 0,
          value: '未处置'
        },
        {
          code: 1,
          value: '已处置'
        },
      ]
    })
    this.data.page = 0
      this.getTaskLists()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.isMore) {
      this.getTaskLists()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})