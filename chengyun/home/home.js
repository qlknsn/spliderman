// pages/homeSecond/homeSecond.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    btnlist: [],

  },
  toDetail: function (e) {
    // wx.navigateTo({
    //   url: '/chengyun/laojietaskList/taskList?type=99' + '&title=' + e.currentTarget.dataset.title
    // })
    wx.navigateTo({
      url: e.currentTarget.dataset.url + '&title=' + e.currentTarget.dataset.title
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      console.log(options)
      wx.setNavigationBarTitle({
        title: options.name
      })
      let modules = JSON.parse(wx.getStorageSync('modules'))
      console.log(modules)
      let arr = modules.filter(fun => {
        return fun.parentFuncitonId == options.type
      })
      this.setData({
        btnlist: arr
      })
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})